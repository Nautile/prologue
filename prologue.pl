% PARTIE 1

% personnage = NOM GROUPE FONCTION NOTE XP 

% ffvi = PERSONNAGE CHEF

ffvii(personnage(aeris,cetras,soin,19,10000),ifalna).
ffvii(personnage(ifalna,cetras,pnj,6,15000),cloud).
ffvii(personnage(cloud,soldat,heros,18,30000),cloud).
ffvii(personnage(tifa,equipe,guerrier,11,15000),barret).
ffvii(personnage(barret,equipe,guerrier,7,14000),cloud).
ffvii(personnage(rougexiii,equipe,soin,18,11000),barret).
ffvii(personnage(yuffie,equipe,ninja,8,2000),barret).

% Reponse a la question a
get_groupe(PERSONNAGE,GROUPE):-ffvii(personnage(PERSONNAGE,GROUPE,_,_,_),_).

% Reponse a la question b
get_chef(PERSONNAGE,CHEF):-ffvii(personnage(PERSONNAGE,_,_,_,_),CHEF).

% Reponse a la question c
/*
	Si on ecrit membre_valide(cloud) le programme retournera true
	en ecrivant la seconde ligne de definition, on demande de chercher le chef du chef jusqu'a arriver sur cloud
*/
membre_valide(cloud).
membre_valide(PERSONNAGE):-get_chef(PERSONNAGE,CHEF),membre_valide(CHEF).

% Reponse a la question d
get_xp(PERSONNAGE,XP):-ffvii(personnage(PERSONNAGE,_,_,_,XP),_).

% Reponse a la question e 1 
% Recupere a note de du personnage
get_note(PERSONNAGE,NOTE):-ffvii(personnage(PERSONNAGE,_,_,NOTE,_),_).

% si la note est du personnage est inferieur a 10 alors j'affiche l'xp de celui-ci.
get_xp_reel(PERSONNAGE,XP):-get_note(PERSONNAGE,NOTE), NOTE < 10, get_xp(PERSONNAGE,XP).

% si la note du personnage est supperieur a 10, alors j'ajoute 5000 a celui-ci
get_xp_reel(PERSONNAGE,XP):-get_note(PERSONNAGE,NOTE), NOTE >= 10, get_xp(PERSONNAGE,XPPERSONNAGE), XP is XPPERSONNAGE+5000.

% Reponse a la question e 2
/*
	je recupere l'xp reel du personnage
	je recupere le chef
	je recupere l'xp reel du chef
	si l'xp du personnage est inferieur a l'xp du chef, alors son xp est bonne je l'affiche
	si l'xp du personnage est supperieur a l'xp du chef, alors son xp est equivalente a celle du chef
*/

get_xp_reel2(PERSONNAGE,XP):-get_xp_reel(PERSONNAGE,XPPERSONNAGE), get_chef(PERSONNAGE,CHEF), get_xp_reel(CHEF,XPCHEF), XPPERSONNAGE < XPCHEF , XP is XPPERSONNAGE.
get_xp_reel2(PERSONNAGE,XP):-get_xp_reel(PERSONNAGE,XPPERSONNAGE), get_chef(PERSONNAGE,CHEF), get_xp_reel(CHEF,XPCHEF), XPPERSONNAGE >= XPCHEF , XP is XPCHEF.



% PARTIE 2 

/*
	X = 0 est la condition d'arret de la recurcive
	sinon on calcule la valeur X-1
	un factorielle est forcement positif donc on renvoit false si X est negatif
*/

factorielle(X,RESULTAT):-X = 0, RESULTAT is 1.
factorielle(X,RESULTAT):- X > 0, Y is X-1, factorielle(Y,PSEUDORESULTAT), RESULTAT is PSEUDORESULTAT*X.



% PARTIE 3

% la suite Lucas est composé de la suite u et la suite V

/*
	la suite u est definit par :
		U0(P,Q) = 0
		U1(P,Q) = 1
		Un(P,Q) = P*U(n-1)(P,Q) - Q*U(n-2)(P,Q)
		
	on a donc les 2 premieres lignes qui permettent de retrouver U0 et U1 (qui sont les conditions d'arret)
	et la derniere ligne correspond a la recurcivite liee a n
*/
u(N,P,Q,RESULT):-N = 0, RESULT is 0.
u(N,P,Q,RESULT):-N = 1, RESULT is 1.
u(N,P,Q,RESULT):-N > -1, X is N-1, Y is N-2, u(X,P,Q,RES1), u(Y,P,Q,RES2), RESULT is P*RES1-Q*RES2.

/*
	la suite v est definit par :
		V0(P,Q) = 2
		V1(P,Q) = P
		Vn(P,Q) = P*V(n-1)(P,Q) - Q*V(n-2)(P,Q)
	
	on a donc les 2 premieres lignes qui permettent de retrouver V0 et V1 (qui sont les conditions d'arret)
	et la derniere ligne correspond a la recurcivite liee a n
*/
v(N,P,Q,RESULT):-N = 0, RESULT is 2.
v(N,P,Q,RESULT):-N = 1, RESULT is P.
v(N,P,Q,RESULT):-N > -1, X is N-1, Y is N-2, v(X,P,Q,RES1), v(Y,P,Q,RES2), RESULT is P*RES1-Q*RES2.


/*
	Deport les calcules des suites U et pour permettre la recurcivite
	
	MAX nombre de boucle
	N : nb d'iteration a laquelle on etait rendu
	P et Q : variable pour les calcules
*/
calc(MAX,N,P,Q):-
N < MAX,
u(N,P,Q,RESULTU),
write( "\nPour N : " + N ),
write( "\nPour P : " + P ),
write( "\nPour Q : " + Q ),
write( "\nU --> " + RESULTU ),
v(N,P,Q,RESULTV),
write( "\nV --> " + RESULTV ),
NBIS is N+1,
calc(MAX,NBIS,P,Q).

/*
	Verifie si la condition P*P-4*Q est different de 0,
	Verification ici pour qu'elle ne soit effectue qu'une seule fois.
	lance les calcules des suites U et V
*/
lucas(P,Q):-
RESULTAT is P*P-4*Q,
not( RESULTAT = 0 ),
calc(20,0,P,Q).
