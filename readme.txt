Ce readMe a pour but d'expliquer le fonctionnement du programme prolog.pl.

lancé le programme avec SWI-Prolog.

1) Ah, Final Fantasy VII…
	a) groupe/2 : trouver le groupe dans lequel une personne évolue.
		get_groupe(aeris,GROUPE).
	b) chef/2 : étant donné le nom d'une personne, trouver qui est le chef du groupe dans lequel elle évolue.
		get_chef(tifa,CHEF).
	c) membre_valide/1 : la structure de l’équipe étant hiérarchique, on doit pouvoir remonter depuis n'importe quel membre vers le chef (Cloud). Le prédicat membre_valide permettra de vérifier qu'un membre est bien sous les ordres de Cloud en remontant la chaine hiérarchique.
		membre_valide(yuffie).
	d) xp/2 : donne le niveau d’experience d'un membre
		get_xp(ifalna,XP).
	e) xp_reelle/2 : donne le niveau d’expérience d'un membre en ajoutant au niveau de base un bonus, en utilisant les règles suivantes :
		i) tous les membres ayant une côte de popularité de 10 ou plus ont un bonus de 5000.
			get_xp_reel(rougexiii,XP).
		ii) Aucun personnage ne peut avoir plus d’expérience que son chef de groupe (attention le cas de Cloud est évidemment spécial).
			get_xp_reel2(rougexiii,XP).	

2) Vous avez dit récursivité ?

	factorielle(4,RESULTAT).

3) Suite de Lucas

	lucas(2,4).